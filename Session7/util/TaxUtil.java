package com.talentsprint.employeepayrollsystem.util;

import com.talentsprint.employeepayrollsystem.entity.Employee;
//import com.talentsprint.employeepayrollsystem.entity.Manager;
//import com.talentsprint.employeepayrollsystem.entity.Sourcing;
//import com.talentsprint.employeepayrollsystem.entity.Trainer;

public class TaxUtil {

	public double calculateTax(Employee Emp) {
		double D =  Emp.calculateGrossSalary();
		if(D > 30000)
			return D * 20.0 / 100.0;
		else
			return D * 0.05;
	}

	/*public double calculateTax(Manager mgr) {
		throw new UnsupportedOperationException();
	}

	public double calculateTax(Trainer trn) {
		throw new UnsupportedOperationException();
	}

	public double calculateTax(Sourcing scr) {
		throw new UnsupportedOperationException();
	}*/

}
