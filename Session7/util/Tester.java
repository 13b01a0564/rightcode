package com.talentsprint.employeepayrollsystem.util;

import com.talentsprint.employeepayrollsystem.entity.Employee;
import com.talentsprint.employeepayrollsystem.entity.Manager;
import com.talentsprint.employeepayrollsystem.entity.Sourcing;
import com.talentsprint.employeepayrollsystem.entity.Trainer;

public class Tester {
public static void main(String[] args)
{
	TaxUtil T = new TaxUtil();
	Manager mgr = new Manager();
	Trainer trn = new Trainer();
	Sourcing scr = new Sourcing();
	Employee emp = new Employee();
	double d = T.calculateTax(emp);
	double a = T.calculateTax(scr);
	double b = T.calculateTax(trn);
	double c = T.calculateTax(mgr);
	System.out.println(a);
	System.out.println(b);
	System.out.println(c);
	System.out.println(d);
}
}
