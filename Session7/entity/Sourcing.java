package com.talentsprint.employeepayrollsystem.entity;

public class Sourcing extends Employee {

	private int enrollmentTarget;
	private int enrollmentReached;
	private double perkPerEnrollment;

	public int getEnrollmentTarget() {
		return enrollmentTarget;
	}

	public void setEnrollmentTarget(int enrollmentTarget) {
		if (enrollmentTarget < 0) {
			return;
		}
		this.enrollmentTarget = enrollmentTarget;
	}

	public int getEnrollmentReached() {
		return enrollmentReached;
	}

	public void setEnrollmentReached(int enrollmentReached) {
		if (enrollmentReached < 0) {
			return;
		}
		this.enrollmentReached = enrollmentReached;
	}

	public double getPerkPerEnrollment() {
		return perkPerEnrollment;
	}

	public void setPerkPerEnrollment(double perkPerEnrollment) {
		if (perkPerEnrollment < 0) {
			return;
		}
		this.perkPerEnrollment = perkPerEnrollment;
	}

	/**
	 * Default constructor
	 */
	public Sourcing() {
		super();
		this.enrollmentReached = 0;
		this.enrollmentTarget = 0;
		this.perkPerEnrollment = 0.0;
	}

	/**
	 * Parameter constructor
	 * 
	 * @param id
	 * @param name
	 * @param basicSalary
	 * @param HRAPer
	 * @param DAPer
	 * @param enrollmentTarget
	 * @param enrollmentReached
	 * @param perkPerEnrollment
	 */
	public Sourcing(int id, String name, double basicSalary, double HRAPer,
			double DAPer, int enrollmentTarget, int enrollmentReached,
			double perkPerEnrollment) {
		super(id, name, basicSalary, HRAPer, DAPer);
		this.enrollmentTarget = enrollmentTarget;
		this.enrollmentReached = enrollmentReached;
		this.perkPerEnrollment = perkPerEnrollment;
	}

	public double calculateGrossSalary() {
		return this.basicSalary
				+ this.DAPer
				+ this.HRAPer
				+ ((((double) this.enrollmentReached / (double) this.enrollmentTarget) * 100) * this.perkPerEnrollment);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + enrollmentReached;
		result = prime * result + enrollmentTarget;
		long temp;
		temp = Double.doubleToLongBits(perkPerEnrollment);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sourcing other = (Sourcing) obj;
		if (enrollmentReached != other.enrollmentReached)
			return false;
		if (enrollmentTarget != other.enrollmentTarget)
			return false;
		if (Double.doubleToLongBits(perkPerEnrollment) != Double
				.doubleToLongBits(other.perkPerEnrollment))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Sourcing [enrollmentTarget=" + enrollmentTarget
				+ ", enrollmentReached=" + enrollmentReached
				+ ", perkPerEnrollment=" + perkPerEnrollment + ", id=" + id
				+ ", name=" + name + ", basicSalary=" + basicSalary
				+ ", HRAPer=" + HRAPer + ", DAPer=" + DAPer + "]";
	}

}
