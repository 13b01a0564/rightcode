package com.talentsprint.employeepayrollsystem.entity;

public class Trainer extends Employee {

	private int batchCount;
	private double perkPerBatch;

	public int getBatchCount() {
		return batchCount;
	}

	public void setBatchCount(int batchCount) {
		if (batchCount < 0) {
			return;
		}
		this.batchCount = batchCount;
	}

	public double getPerkPerBatch() {
		return perkPerBatch;
	}

	public void setPerkPerBatch(double perkPerBatch) {
		if (perkPerBatch < 0) {
			return;
		}
		this.perkPerBatch = perkPerBatch;
	}

	/**
	 * Default constructor
	 */
	public Trainer() {
		super();
		this.batchCount = 0;
		this.perkPerBatch = 0.0;
	}

	/**
	 * Parameter constructor
	 * 
	 * @param id
	 * @param name
	 * @param basicSalary
	 * @param HRAPer
	 * @param DAPer
	 * @param batchCount
	 * @param perkPerBatch
	 */
	public Trainer(int id, String name, double basicSalary, double HRAPer,
			double DAPer, int batchCount, double perkPerBatch) {

		super(id, name, basicSalary, HRAPer, DAPer);
		this.batchCount = batchCount;
		this.perkPerBatch = perkPerBatch;
	}

	public double calculateGrossSalary() {
		return this.basicSalary + this.DAPer + this.HRAPer
				+ (this.batchCount * this.perkPerBatch);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + batchCount;
		long temp;
		temp = Double.doubleToLongBits(perkPerBatch);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trainer other = (Trainer) obj;
		if (batchCount != other.batchCount)
			return false;
		if (Double.doubleToLongBits(perkPerBatch) != Double
				.doubleToLongBits(other.perkPerBatch))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Trainer [batchCount=" + batchCount + ", perkPerBatch="
				+ perkPerBatch + ", id=" + id + ", name=" + name
				+ ", basicSalary=" + basicSalary + ", HRAPer=" + HRAPer
				+ ", DAPer=" + DAPer + "]";
	}

}
