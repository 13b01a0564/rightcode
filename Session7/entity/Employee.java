package com.talentsprint.employeepayrollsystem.entity;

public class Employee {

	protected int id;
	protected String name;
	protected double basicSalary;
	protected double HRAPer;
	protected double DAPer;

	/************* getters and setters *******************************************************/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		if (id < 0) {
			return;
		}
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if ((name == null) || (!name.matches("[a-zA-Z\\s]*"))) {
			return;
		}
		this.name = name;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		if (basicSalary < 0) {
			return;
		}
		this.basicSalary = basicSalary;
	}

	public double getHRAPer() {
		return HRAPer;
	}

	public void setHRAPer(double hRAPer) {
		if (hRAPer < 0) {
			return;
		}
		HRAPer = hRAPer;
	}

	public double getDAPer() {
		return DAPer;
	}

	public void setDAPer(double dAPer) {
		if (dAPer < 0) {
			return;
		}
		DAPer = dAPer;
	}

	/**
	 * Default constructor
	 */
	public Employee() {
		this.id = 0;
		this.name = null;
		this.basicSalary = 0.0;
		this.HRAPer = 0.0;
		this.DAPer = 0.0;
	}

	/**
	 * Parameter constructor
	 * 
	 * @param id
	 * @param name
	 * @param basicSalary
	 * @param HRAPer
	 * @param DAPer
	 */
	public Employee(int id, String name, double basicSalary, double HRAPer,
			double DAPer) {
		this.id = id;
		this.name = name;
		this.basicSalary = basicSalary;
		this.HRAPer = HRAPer;
		this.DAPer = DAPer;
	}

	public double calculateGrossSalary() {
		return this.basicSalary + this.DAPer + this.HRAPer;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", basicSalary="
				+ basicSalary + ", HRAPer=" + HRAPer + ", DAPer=" + DAPer + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(DAPer);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(HRAPer);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(basicSalary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (Double.doubleToLongBits(DAPer) != Double
				.doubleToLongBits(other.DAPer))
			return false;
		if (Double.doubleToLongBits(HRAPer) != Double
				.doubleToLongBits(other.HRAPer))
			return false;
		if (Double.doubleToLongBits(basicSalary) != Double
				.doubleToLongBits(other.basicSalary))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
