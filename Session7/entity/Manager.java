package com.talentsprint.employeepayrollsystem.entity;

public class Manager extends Employee {

	private double projectAllowance;

	public double getProjectAllowance() {
		return projectAllowance;
	}

	public void setProjectAllowance(double projectAllowance) {
		if (projectAllowance < 0) {
			return;
		}
		this.projectAllowance = projectAllowance;
	}

	/**
	 * Default constructor
	 */
	public Manager() {
		super();
		this.projectAllowance = 0.0;
	}

	/**
	 * Parameter constructor
	 * 
	 * @param id
	 * @param name
	 * @param basicSalary
	 * @param HRAPer
	 * @param DAPer
	 * @param projectAllowance
	 */
	public Manager(int id, String name, double basicSalary, double HRAPer,
			double DAPer, double projectAllowance) {
		super(id, name, basicSalary, HRAPer, DAPer);
		this.projectAllowance = projectAllowance;
	}

	public double calculateGrossSalary() {
		return this.basicSalary + this.DAPer + this.HRAPer
				+ this.projectAllowance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(projectAllowance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manager other = (Manager) obj;
		if (Double.doubleToLongBits(projectAllowance) != Double
				.doubleToLongBits(other.projectAllowance))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Manager [projectAllowance=" + projectAllowance + ", id=" + id
				+ ", name=" + name + ", basicSalary=" + basicSalary
				+ ", HRAPer=" + HRAPer + ", DAPer=" + DAPer + "]";
	}

}
