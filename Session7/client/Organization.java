package com.talentsprint.employeepayrollsystem.client;

import com.talentsprint.employeepayrollsystem.entity.Employee;

public class Organization {

	private String name;
	private Employee[] employeeList;
	private int noOfEmployees;

	/******************* GETTER AND SETTERS *************************************/

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if ((name == null) || (!name.matches("[a-zA-Z\\s]*"))) {
			return;
		}
		this.name = name;
	}

	public Employee[] getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(Employee[] employeeList) {
		this.employeeList = employeeList;
	}

	public int getNoOfEmployees() {
		return noOfEmployees;
	}

	public void setNoOfEmployees(int noOfEmployees) {
		if (noOfEmployees < 0) {
			return;
		}
		this.noOfEmployees = noOfEmployees;
	}

	/**
	 * Default constructor. Initializes the array to a size of 10
	 */
	public Organization() {
		this.employeeList = new Employee[10];
		this.noOfEmployees = 0;
	}

	/**
	 * This method adds 5 employees into the employeeList of the class. This
	 * method overwrites the first 5 elements of the array. Should be used only
	 * for initial population.
	 */
	public void addEmployeeBulk() {

	}

	/**
	 * This method prints all the employees in the list on the console
	 */
	public void printAllEmployees() {

	}

	/**
	 * Add an employee if ID is not in the list increment noOfEmployees field
	 * 
	 * @param emp
	 * @return
	 */
	public int addEmployee(Employee emp) {
		throw new UnsupportedOperationException();
	}

	/**
	 * This method searches the array for an employee whose name is equal to the
	 * given name Return employee if found, null otherwise
	 * 
	 * @param name
	 * @return
	 */
	public Employee searchEmployeeByName(String name) {
		throw new UnsupportedOperationException();

	}

	/**
	 * This method searches the array for an employee whose name contains the
	 * given partName Return employee if found, null otherwise
	 * 
	 * @param partName
	 * @return
	 */
	public Employee searchEmployeeByPartOfName(String partName) {
		throw new UnsupportedOperationException();

	}

	/**
	 * 
	 * This method returns the employee with the given Id, return null if not
	 * found
	 * 
	 * @param id
	 * @return
	 */
	public Employee getEmployeeById(int id) {
		throw new UnsupportedOperationException();

	}

	/**
	 * This method searches for an employee with the given id. If found, it sets
	 * the employee object to null, moves up the rest of the employees below
	 * this one in the array and decrements the no of employees. It then returns
	 * true. If not found, it simply returns false.
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteEmployee(int id) {
		throw new UnsupportedOperationException();

	}

}
