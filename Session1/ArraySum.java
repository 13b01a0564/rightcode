package com.talentsprint;

public class ArraySum {
	
	public int getSum(int[] inputArray) {
		int sum = 0;
		if((inputArray != null) && inputArray.length > 0){
			for(int i = 0;i < inputArray.length;i++){
				sum += inputArray[i];
			}
			return sum;
		}
		else{
			return -1;
		}

		//throw new UnsupportedOperationException();
	}
	public static void main(String[] args){
		ArraySum as = new ArraySum();
		int [] a = new int[args.length];
		int i = 0;
		for(String n :args){
			a[i] = Integer.parseInt(n);
			i++;
		}		
		int sum = as.getSum(a);
		System.out.println(sum);
		
	}

}

