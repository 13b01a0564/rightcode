package com.talentsprint;

import java.util.Scanner;

public class CharCounter {

    public int countOccurence(String input, char toFind) {
        // throw new UnsupportedOperationException();
        int count = 0;
        if (input != null && input != "") {
            input = input.toLowerCase();
            for (int i = 0; i < input.length(); i++) {
                if (Character.toLowerCase(toFind) == input.charAt(i)) {
                    count++;
                }
            }
            // return count;
        } else {
            count = -1;
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        CharCounter cc = new CharCounter();
        try {
            String input = a.nextLine();
            char c = a.nextLine().charAt(0);
            System.out.println(cc.countOccurence(input, c));
        } catch (Exception e) {
            System.out.println("-1");
        }
    }
}
