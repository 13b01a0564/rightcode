package com.talentsprint;

public class FizzBizz {
	
		public String getOutputString(int number){
			if(number > 0 ) {
				if(((number %3) == 0) && (number % 5) != 0 )
					return ("Fizz");
				else if(((number % 5)== 0)&& ((number % 3)!= 0))
					return ("Bizz");
				else if(((number % 5)== 0)&&((number % 3)==0)||(number == 1))
					return ("FizzBizz");
				else 
					return (""+number);
			}
			else
				return ("Error");
				
		}
		public static void main(String[] args) {
			FizzBizz F = new FizzBizz();
			try{
				int number = Integer.parseInt(args[0]);
				if(args.length == 1)
					System.out.println(F.getOutputString(number));
				else
					System.out.println("Error");
			} catch(Exception e ) {
				System.out.println("Error");
			}
			
		}
}


