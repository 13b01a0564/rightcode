package com.talentsprint;

public class Student {

	protected String name;
	protected int studentId;
	protected double examFee;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public double getExamFee() {
		return examFee;
	}

	public void setExamFee(double examFee) {
		this.examFee = examFee;
	}

	/**
	 * Default constructor
	 */
	public Student() {
		this.name = null;
		this.studentId = 0;
		this.examFee = 0;
	}

	/**
	 * All-fields constructor
	 * 
	 * @param name
	 * @param studentId
	 * @param examFee
	 */
	public Student(String name, int studentId, double examFee) {
		this.name = name;
		this.studentId = studentId;
		this.examFee = examFee;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", studentId=" + studentId
				+ ", examFee=" + examFee + "]";
	}

	/**
	 * Display details of the student in the form Student [name=John Smith,
	 * studentId=123, examFee=100.0] *
	 * 
	 * @return
	 */
	public String displayDetails() {
		return this.toString();
	}

	/**
	 * Method that takes an amount and returns the remaining fees to be paid The
	 * remaining fees could be positive, negative(in case excess is paid) or
	 * zero
	 * 
	 * @param amount
	 * @return
	 */
	public double payFee(double amount) {
		double toPay = this.examFee - amount;
		return toPay;
	}

}
class DayScholar extends Student
{
	double transportFee;
	DayScholar() {
	}
	DayScholar(double transportFee)
	{
		this.transportFee = transportFee;
	}
	public void setTransportFee(double transportFee) {
		this.transportFee = transportFee;
	}
	public double getTransportFee() {
		return transportFee;
	}
	public String displayDetails() {
		return this.toString();
	}
	public double payFee(double amount) {
		double toPay = this.transportFee - amount;
		return toPay;
	}
}
class Hosteller extends Student 
{
	double hostelFee;
	Hosteller() {	
	}
	Hosteller(double hostelFee)
	{
		this.hostelFee = hostelFee;
	}
	public void setHostelFee(double hostelFee) {
		this.hostelFee = hostelFee;
	}
	public double getHostelFee() {
		return hostelFee;
	}
	public String displayDetails() {
		return this.toString();
	}
	public double payFee(double amount) {
		double toPay = this.hostelFee - amount;
		return toPay;
	}
}